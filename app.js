new Vue({
  el: '#exercise',
  data: {
    effectClass: '',
    userClass: '',
    className: 'red',
    border: '',
    inputStyle: '',
    percent: 0
  },
  methods: {
    startEffect: function() {
      var vm = this;
+      setInterval(function() {
        vm.effectClass = vm.effectClass === 'highlight' ? 'shrink' : 'highlight';
      }, 1000);
    },
    startProgress: function() {
      this.percent += 10;
      var vm = this;
      setTimeout(function() {
        if (vm.percent < 100) vm.startProgress();
      }, 100)
    }
  },
  computed: {
    hasBorder: function() {
      return this.border === 'true';
    }
  }
});
